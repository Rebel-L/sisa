Projects CHANGELOG
==================

This file is used to list changes made in each version of the Projects cookbook.

2.1.1
-----

* Rebel L - [Issue #6](https://bitbucket.org/Rebel-L/sisa/issues/6/ssl-support-for-nginx) Added SSL support 

2.0.0
-----

* Rebel L - [Issue #23](https://bitbucket.org/Rebel-L/sisa/issues/23/nginx-project-type-service-duplicate) Fixed the duplicate location error

1.6.0
-----

* Added support for PHP versions 5.5, 5.6 and 7.0
* Optimizations for PHP/Nginx Setup from [Issue #17](https://bitbucket.org/Rebel-L/sisa/issues/17)
    * Supports now type **php-www** for professional setup
    * Supports now type **php-simple** for simply using same domain for assets and images
* Bugfix [Issue #14](https://bitbucket.org/Rebel-L/sisa/issues/14)

1.3.3
-----

- Rebel-L - Added nginx service configuration

0.1.0
-----

- Rebel-L - Initial release of Projects

