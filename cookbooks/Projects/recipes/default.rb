#
# Cookbook Name:: Projects
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "System::default"

#
# Skip recipe if no projects are configured
#
if !node.attribute?('projects')
	Chef::Log.warn('No projects configured!')
	return
end

#
# Create servers for each project
#
node['projects'].each do |project|
	Chef::Log.info('Setup Project: ' + project['name'] + ' as type: ' + project['type'])
	#
    # Create Server
    #
    case project['type']
    when 'redirect'
        scheme = '$scheme'
        if project['ssl'] == true
            scheme = 'https'
        end
    	template "/etc/nginx/sites-available/#{project['name']}" do
			source "server/redirect-site.erb"
			mode "0644"
			owner "root"
			group "root"
			notifies :reload, resources(:service => 'nginx')
			variables ({
				:name			=> project['name'],
				:type			=> project['type'],
				:server_name	=> project['server_name'],
				:target			=> project['target'],
				:scheme         => scheme
			})
		end
	when 'service'
		template "/etc/nginx/sites-available/#{project['name']}" do
			source "server/service-site.erb"
			mode "0644"
			owner "root"
			group "root"
			notifies :reload, resources(:service => 'nginx')
			variables ({
				:name			    => project['name'],
				:type			    => project['type'],
				:server_name	    => project['server_name'],
				:root			    => project['root'],
                :index		    	=> project['index']
			})
		end
    when 'php-www'
		template "/etc/nginx/sites-available/#{project['name']}" do
			source "server/php-www.erb"
			mode "0644"
			owner "root"
			group "root"
			notifies :reload, resources(:service => 'nginx')
			variables ({
				:name			=> project['name'],
				:type			=> project['type'],
				:server_name	=> project['server_name'],
				:root			=> project['root'],
				:index			=> project['index']
			})
		end
	when 'php-ssl'
        template "/etc/nginx/sites-available/#{project['name']}" do
            source "server/php-ssl.erb"
            mode "0644"
            owner "root"
            group "root"
            notifies :reload, resources(:service => 'nginx')
            variables ({
                :name			        => project['name'],
                :type			        => project['type'],
                :server_name	        => project['server_name'],
                :root			        => project['root'],
                :index			        => project['index'],
                :certificate_root       => node['Projects']['certificate_root'],
                :certificate_filename   => node['Projects']['certificate_filename'],
                :certificate_keyname    => node['Projects']['certificate_keyname']
            })
        end
	when 'php-simple'
        template "/etc/nginx/sites-available/#{project['name']}" do
            source "server/php-simple.erb"
            mode "0644"
            owner "root"
            group "root"
            notifies :reload, resources(:service => 'nginx')
            variables ({
                :name			=> project['name'],
                :type			=> project['type'],
                :server_name	=> project['server_name'],
                :root			=> project['root'],
                :index			=> project['index']
            })
        end
    end

    #
    # Activate Server
    #
    link "/etc/nginx/sites-enabled/#{project['name']}" do
    	action :create
    	link_type :symbolic
    	to "/etc/nginx/sites-available/#{project['name']}"
    	notifies :reload, resources(:service => 'nginx')
    end
end