# No default attributes so far
default['nginx']['loglevel']		= 'error'
default['nginx']['cache_active']	= 'true'
default['nginx']['cache_lifetime']	= '1h'

# ssl configurations
default['Projects']['certificate_root']     = '/etc/letsencrypt/live'
default['Projects']['certificate_filename'] = 'fullchain.pem'
default['Projects']['certificate_keyname']  = 'privkey.pem'