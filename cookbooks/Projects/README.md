Projects Cookbook
=================
This cookbooks set up your PHP projects with nginx.

Requirements
------------
#### packages
- `Nginx` - Projects needs Nginx webserver included by the Nginx cookbook.
- `PHP` - Projects needs PHP included by the Php cookbook.

Attributes
----------
#### Projects::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['nginx']['loglevel']</tt></td>
    <td>String</td>
    <td>The Nginx log level.</td>
    <td><tt>error</tt></td>
  </tr>
  <tr>
    <td><tt>['nginx']['cache_active']</tt></td>
    <td>String</td>
    <td>Activates the Nginx cache.</td>
    <td><tt>true</tt></td>
  </tr>
  <tr>
    <td><tt>['nginx']['cache_lifetime']</tt></td>
    <td>String</td>
    <td>The Nginx cache lifetime.</td>
    <td><tt>1h</tt></td>
  </tr>
  <tr>
    <td><tt>['name']</tt></td>
    <td>String</td>
    <td>The name of the project. Used for naming the server config files etc.</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['type']</tt></td>
    <td>String</td>
    <td>The type of the project. Can be php-www, php-ssl, php-simple, redirect or service.</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['server_name']</tt></td>
    <td>String</td>
    <td>The nginx server_name to listen to.</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['root']</tt></td>
    <td>String</td>
    <td>Your projects root directory. Not needed for type 'redirect'.</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['index']</tt></td>
    <td>String</td>
    <td>The index file/script to point to as main entry point. Not needed for type 'redirect'.</td>
    <td><tt></tt></td>
  </tr>
  <tr>
    <td><tt>['target']</tt></td>
    <td>String</td>
    <td>The target to redirect to. Not needed for type 'php' or 'service'.</td>
    <td><tt></tt></td>
  </tr>
  <tr>
      <td><tt>['Projects']['certificate_root']</tt></td>
      <td>String</td>
      <td>The root folder where the certificates and keys are stored.</td>
      <td><tt>/etc/letsencrypt/live</tt></td>
  </tr>
  <tr>
      <td><tt>['Projects']['certificate_filename']</tt></td>
      <td>String</td>
      <td>The filename of the certificates.</td>
      <td><tt>fullchain.pem</tt></td>
  </tr>
  <tr>
      <td><tt>['Projects']['certificate_keyname']</tt></td>
      <td>String</td>
      <td>The name of the keyfile.</td>
      <td><tt>privkey.pem</tt></td>
  </tr> 
</table>

Usage
-----
#### Projects::default
Just include `Projects` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Projects]"
  ]
}
```

and configure it with a JSON, e.g.:

```
'projects': [
    {
        'name': 'bitbucket_proxy',
        'type': 'service',
        'server_name': 'bitbucket.proxy.vm',
        'root': '/vagrant/public',
        'index': 'index.php'
    }
]
```

Contributing
------------
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
