# Docker CHANGELOG

This file is used to list changes made in each version of the Docker cookbook.

## 2.3.1
- Rebel L - Made docker compose version configurable. Default version is 1.16.1

## 2.2.1
- Rebel L - Added docker compose

## 2.2.0
- Rebel L - [Issue #25](https://bitbucket.org/Rebel-L/sisa/issues/25/create-docker-cookbook) Initial release of Docker