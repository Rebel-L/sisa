#
# Cookbook Name:: Docker
# Recipe:: compose
#
# Copyright 2017, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "add docker compose" do
	user	"root"
	code	<<-EOH
	   curl -L https://github.com/docker/compose/releases/download/#{node['Docker']['compose']['version']}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
       chmod +x /usr/local/bin/docker-compose
	EOH
	not_if "docker-compose version | grep #{node['Docker']['compose']['version']}"
end

