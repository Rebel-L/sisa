#
# Cookbook Name:: Docker
# Recipe:: default
#
# Copyright 2017, Rebel L
#
# All rights reserved - Do Not Redistribute
#

bash "add docker gpg key" do
	user	"root"
	code	<<-EOH
	   sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
	EOH
	not_if "sudo apt-key list | grep docker"
end

bash "add docker to source list" do
	user	"root"
	code	<<-EOH
	   sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'
	EOH
	not_if "grep docker /etc/apt/sources.list"
end

bash "update source list and clear cache" do
    user    "root"
	code    <<-EOH
		sudo apt-get update
		apt-cache policy docker-engine
	EOH
end

package "docker-engine" do
	action :install
end

bash "add docker group to user vagrant" do
	user	"root"
	code	<<-EOH
	   usermod -aG docker vagrant
	EOH
	not_if "groups | grep docker"
end

include_recipe "Docker::compose"
