NewRelic Cookbook
=================
This cookbooks installs the new relic monitoring demon.

Requirements
------------
#### operating systems
- `Ubuntu` - NewRelic needs Ubuntu 14.04 LTS or 14.10.

#### license key
- you need a valid license key from NewRelic. If you don't have anyone, just register at https://newrelic.com/.

Attributes
----------
#### NewRelic::default
<table>
	<tr>
		<td><tt>['NewRelic']['licensekey']</tt></td>
		<td>String</td>
		<td>The new relic license key to install.</td>
		<td><tt>'your_license_key'</tt></td>
	</tr>
	<tr>
		<td><tt>['NewRelic']['loglevel']</tt></td>
		<td>String</td>
		<td>The log level new relic is writing in its log.</td>
		<td><tt>'error'</tt></td>
	</tr>
	<tr>
		<td><tt>['NewRelic']['logrotate']['interval']</tt></td>
		<td>String</td>
		<td>The interval for the new relic logrotation.</td>
		<td><tt>'daily'</tt></td>
	</tr>
    <tr>
		<td><tt>['NewRelic']['logrotate']['number_of_files']</tt></td>
		<td>String</td>
		<td>The number of logfiles to keep with logrotation.</td>
		<td><tt>'10'</tt></td>
	</tr>
</table>

Usage
-----
#### NewRelic::default
Just include `NewRelic` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[NewRelic]"
  ]
}

and add the license key to the attributes.

Contributing
------------
(optional) If this is a public cookbook, detail the process for contributing. If this is a private cookbook, remove this section.

e.g.
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
