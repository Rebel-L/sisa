#
# Cookbook Name:: NewRelic
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# Check that license key is set, otherwise throw an error
#
if node['NewRelic']['licensekey'] == 'your_license_key'
	log "no license key for new relic set" do
		level :error
	end
end

#
# Add new relic repository to apt-get
#
execute "add new relic to source list" do
	command <<-EOH
		echo deb http://apt.newrelic.com/debian/ newrelic non-free >> /etc/apt/sources.list.d/newrelic.list
	EOH
	not_if do ::File.exists?('/etc/apt/sources.list.d/newrelic.list') end
end

execute "trust gpg key from new relic" do
	command <<-EOH
		wget -O- https://download.newrelic.com/548C16BF.gpg | apt-key add -
	EOH
	not_if "sudo apt-key list | grep newrelic"
end

execute "update source list" do
	command <<-EOH
		sudo apt-get update
	EOH
end

#
# Install new relic plugin as apt-get package
#
package "newrelic-sysmond" do
	action :install
end

#
# Define Service
#
service "newrelic-sysmond" do
	supports :restart => true
	action :enable
end

#
# Create config file config
#
template "/etc/newrelic/nrsysmond.cfg" do
	source "nrsysmond.cfg.erb"
	mode "0640"
	owner "newrelic"
	group "newrelic"
	notifies :restart, resources(:service => 'newrelic-sysmond')
end

#
# Setup logrotate
#
template "/etc/logrotate.d/newrelic-sysmond" do
	source	"logrotate/newrelic-sysmond.erb"
	mode	"644"
	owner	"root"
	group	"root"
end

#
# Reload logrotate config
#
execute "reload logrotate config" do
	command	<<-EOH
		sudo logrotate -f /etc/logrotate.conf
	EOH
end