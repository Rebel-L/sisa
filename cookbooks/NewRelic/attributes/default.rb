default['NewRelic']['licensekey']					= 'your_license_key'
default['NewRelic']['loglevel']						= 'error'
default['NewRelic']['logrotate']['interval']		= 'daily'
default['NewRelic']['logrotate']['number_of_files']	= '10'