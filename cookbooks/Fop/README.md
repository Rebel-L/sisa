Fop Cookbook
============

This cookbook installs apache fop.

Requirements
------------

Nothing so far.

Attributes
----------

No attributes so far.

Usage
-----
#### Fop::default

Just include `Fop` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Fop]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
