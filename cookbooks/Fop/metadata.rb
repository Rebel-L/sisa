name             'Fop'
maintainer       'Rebel L'
maintainer_email 'dj@rebel-l.net'
license          'All rights reserved'
description      'Installs/Configures Fop'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.7.0'
