ApacheBenchmark CHANGELOG
=========================

This file is used to list changes made in each version of the ApacheBenchmark cookbook.

1.4.0
-----
- Rebel L - Initial release of ApacheBenchmark
