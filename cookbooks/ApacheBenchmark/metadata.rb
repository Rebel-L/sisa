name             'ApacheBenchmark'
maintainer       'Rebel L'
maintainer_email 'dj@rebel-l.net'
license          'All rights reserved'
description      'Installs ApacheBenchmark'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '1.4.0'
