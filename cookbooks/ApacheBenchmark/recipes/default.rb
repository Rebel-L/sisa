#
# Cookbook Name:: ApacheBenchmark
# Recipe:: default
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "System::default"

package "apache2-utils" do
	action :install
end