#
# Cookbook:: ChefDK
# Recipe:: default
#
# Copyright:: 2017, Rebel L, All Rights Reserved.

bash "install ChefDK #{node['ChefDK']['version']}" do
	user	"root"
	code	<<-EOH
		wget -O /tmp/chefdk.deb https://packages.chef.io/files/stable/chefdk/#{node['ChefDK']['version']}/ubuntu/16.04/chefdk_#{node['ChefDK']['version']}-1_amd64.deb
		dpkg -i /tmp/chefdk.deb
	EOH
	not_if	"chef -v | grep #{node['ChefDK']['version']}"
end