# ChefDK Cookbook

This cookbook installs ChefDK.

## Requirements

### Chef

- Chef 12.0 or later


## Attributes

### ChefDK::default

|   Key                     |   Type    |   Description                         |   Default |
|---------------------------|-----------|---------------------------------------|-----------|
|   ['ChefDK']['version']   |   String  |   The The ChefDK version to install.  |   2.3.4   |

## Usage

### ChefDK::default

Just include `ChefDK` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[ChefDK]"
  ]
}
```

## Contributing

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

## License and Authors

Authors: Rebel L

