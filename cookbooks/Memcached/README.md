Memcached Cookbook
==================
This cookbook installs the memcached on your system.

Requirements
------------
No requirements so far.

Attributes
----------

#### Memcached::default
<table>
  <tr>
    <th>memory</th>
    <th>integer</th>
    <th>Size of the memcache memory</th>
    <th>64</th>
  </tr>
  <tr>
      <th>port</th>
      <th>integer</th>
      <th>The port of the memcache</th>
      <th>11211</th>
    </tr>
</table>

Usage
-----
#### Memcached::default
Just include `Memcached` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Memcached]"
  ]
}
```

Contributing
------------
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel-L
