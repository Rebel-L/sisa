#
# Cookbook Name:: Memcached
# Recipe:: default
#
# Copyright 2016, Rebel-L
#
# All rights reserved - Do Not Redistribute
#

#
# Install package
#
package "memcached" do
	action :install
end

#
# Define Service
#
service "memcached" do
	supports :restart => true
	action :enable
end

template "/etc/memcached.conf" do
    source "memcached.conf.erb"
    mode "0644"
    owner "root"
    group "root"
    notifies :restart, resources(:service => 'memcached')
end