#
# Cookbook Name:: LetsEncrypt
# Recipe:: default
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# install letsencrypt
#
package "letsencrypt" do
	action :install
end

#
# create renew cronjob
#
template "/etc/cron.d/letsencrypt" do
	source "letsencrypt.erb"
	mode "0644"
	owner "root"
	group "root"
end

#
# setup domains if not existing
#
node['LetsEncrypt']['domains'].each do |domain|
    bash "install certificate for domain #{domain['dns']}" do
        user "root"
        code <<-EOH
            letsencrypt certonly --webroot -w #{domain['webroot']} -d #{domain['dns']}
        EOH
        not_if { File.exist?("/etc/letsencrypt/live/#{domain['dns']}/") }
    end
end
