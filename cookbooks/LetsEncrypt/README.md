# LetsEncrypt Cookbook

This cookbook installs letsencrypt and creates the certificates for the domains configured.
NOTE: Maybe it's not creating the certificates reliable as there is maybe user interaction needed.

## Requirements

Nothing so far.

### Chef

- Chef 12.0 or later

### Cookbooks

Nothing so far.

## Attributes

### LetsEncrypt::default

|   Key                                             |   Type    |   Description                                                                             |   Default |
|---------------------------------------------------|-----------|-------------------------------------------------------------------------------------------|-----------|
|   ['LetsEncrypt']['domains']                      |   Array   |   The domains to create the certificates for. Must be an object of 'dns' and 'webroot'    |   []      |
|   ['LetsEncrypt']['cron']['minute']               |   Integer |   Time when the renew process is started. It's the minute.                                |   12      |
|   ['LetsEncrypt']['cron']['hour']                 |   Integer |   Time when the renew process is started. It's the hour.                                  |   1       |

## Usage

### LetsEncrypt::default
Just include `LetsEncrypt` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[LetsEncrypt]"
  ]
}
```

and setup the domains like the following:

```json
{
    'LetsEncrypt': {
        'domains': [
            {
                'dns': 'domain1.net',
                'webroot': '/var/www/domain1'
            },
            {
                'dns': 'domain2.org',
                'webroot': '/var/www/domain2'
            }
        ]
    }
}
```

## Contributing

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

## License and Authors

Authors: Rebel L

