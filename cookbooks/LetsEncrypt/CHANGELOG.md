# LetsEncrypt CHANGELOG

This file is used to list changes made in each version of the LetsEncrypt cookbook.

## 2.1.0
- Rebel L - [Issue #6](https://bitbucket.org/Rebel-L/sisa/issues/6/ssl-support-for-nginx) Initial release of LetsEncrypt
