Cups CHANGELOG
==============

This file is used to list changes made in each version of the Cups cookbook.

1.7.0
-----

* Rebel L - Initial release of Cups
