# Redis CHANGELOG

This file is used to list changes made in each version of the Redis cookbook.

## 2.3.0
- Rebel L - [Issue #30](https://bitbucket.org/Rebel-L/sisa/issues/30/redis-cookbook-needed) Initial release of Redis