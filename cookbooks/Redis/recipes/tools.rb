#
# Cookbook:: Redis
# Recipe:: tools
#
# Copyright:: 2017, Rebel L, All Rights Reserved.

package "redis-tools" do
	action :install
end