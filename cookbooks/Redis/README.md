# Redis Cookbook
This cookbook installs Redis.

## Requirements
### Chef
* Chef 12.0 or later

## Attributes
So far there are no attributes.

## Usage

### Redis::tools

Just include `Redis::tools` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Redis::tools]"
  ]
}
```

## Contributing

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

## License and Authors

Authors: Rebel L
