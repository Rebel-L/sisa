name                'Redis'
maintainer          'Rebel-L'
maintainer_email    'dj@rebel-l.net'
license             'All Rights Reserved'
description         'Installs/Configures Redis'
long_description    IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version             '2.3.0'
chef_version        '>= 12.1' if respond_to?(:chef_version)

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
issues_url 'https://bitbucket.org/Rebel-L/sisa/issues?status=new&status=open'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
source_url 'https://bitbucket.org/Rebel-L/sisa/src'
