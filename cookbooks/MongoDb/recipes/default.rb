#
# Cookbook Name:: MongoDb
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# Installs key for mongodb repo and adds repo to source list
#
bash "trust gpg key from mongodb" do
	user "root"
	code "apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927"
	not_if "sudo apt-key list | grep EA312927"
end

execute "add mongodb repositories" do
	user "root"
	command	<<-EOH
		add-apt-repository "deb http://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/3.2 multiverse"
	EOH
	not_if "cat /etc/apt/sources.list | grep mongodb"
end

bash "update source list" do
	user "root"
	code "apt-get update"
end

#
# Install package
#
package "mongodb-org" do
	action :install
	not_if "sudo service mongod status | grep running"
end

#
# Define Service
#
service "mongod" do
	supports :restart => true
	action :enable
end