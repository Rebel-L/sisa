name             'MongoDb'
maintainer       'Rebel L'
maintainer_email 'dj@rebel-l.net'
license          'All rights reserved'
description      'Installs/Configures MongoDb'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
