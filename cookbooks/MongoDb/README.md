MongoDb Cookbook
================
This cookbook installs and configures the MongoDB.

Requirements
------------

No requirements so far.

Attributes
----------

No attributes needed for now.

Usage
-----
#### MongoDb::default
Just include `MongoDb` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[MongoDb]"
  ]
}
```

Contributing
------------

e.g.
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
