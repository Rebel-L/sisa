ElasticSearch Cookbook
======================
This cookbook installs the index server elastic search.

Requirements
------------

#### packages
- `Java` - ElasticSearch needs Java to be installed. Java 8 is recommended.

Attributes
----------
#### ElasticSearch::default
<table>
  <tr>
    <td><tt>['ElasticSearch']['network.host']</tt></td>
    <td>String</td>
    <td>The server to be binded.</td>
    <td><tt>localhost</tt></td>
  </tr>
</table>

Usage
-----
#### ElasticSearch::default
Just include `ElasticSearch` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[ElasticSearch]"
  ]
}
```

Contributing
------------
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
