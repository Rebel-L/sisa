#
# Cookbook Name:: ElasticSearch
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "Java"

#
# Add ElasticSearch repository
#
bash "trust gpg key from elasticsearch" do
	user	"root"
	code	"wget -O - http://packages.elasticsearch.org/GPG-KEY-elasticsearch | apt-key add -"
	not_if "apt-key list | grep Elasticsearch"
end

bash "add elasticsearch repository" do
	user	"root"
	code	"echo 'deb http://packages.elasticsearch.org/elasticsearch/1.4/debian stable main' | tee /etc/apt/sources.list.d/elasticsearch.list"
	not_if	"cat /etc/apt/sources.list.d/* | grep elasticsearch"
end

bash "update source list" do
	user	"root"
	code	"apt-get update"
end

#
# Install ElasticSearch
#
package "elasticsearch" do
	action :install
end

#
# Define Service
#
service "elasticsearch" do
	supports	:restart => true
	action		:enable
end

#
# Configure elasticsearch
#
template "/etc/elasticsearch/elasticsearch.yml" do
	source		"elasticsearch.yml.erb"
	mode		"0644"
	owner		"root"
	group		"root"
	notifies	:restart, resources(:service => 'elasticsearch')
end

#
# Setup start on boot
#
bash "setup elasticsearch startup on boot" do
	user	"root"
	code 	"update-rc.d elasticsearch defaults 95 10"
	not_if	"grep -R elasticsearch /etc/rc*"
end