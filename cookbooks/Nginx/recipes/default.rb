#
# Cookbook Name:: Nginx
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "System::default"

#
# Install package
#
package "nginx" do
	action :install
end

#
# Define Service
#
service "nginx" do
	supports :restart => true, :reload => true
	action :enable
end

#
# Create includes
#
directory "/etc/nginx/includes" do
	owner 'root'
	group 'root'
	mode '0755'
	action :create
end

#
# Configure Nginx
#
template "/etc/nginx/nginx.conf" do
    source "nginx.conf.erb"
	mode "0644"
	owner "root"
	group "root"
end

template "/etc/nginx/conf.d/ssl.conf" do
    source "conf.d/ssl.conf.erb"
	mode "0644"
	owner "root"
	group "root"
end

#
# Create Errorpages Config at Include Path
#
template "/etc/nginx/includes/errorpages" do
	source "includes/errorpages.erb"
	mode "0644"
	owner "root"
	group "root"
end

#
# Create TrailingSlash Config at Include Path
#
template "etc/nginx/includes/trailingslash" do
	source	"includes/trailingslash.erb"
	mode	"664"
	owner	"root"
	group	"root"
end

#
# Create SSL Server Config at Include Path
#
template "etc/nginx/includes/ssl-server" do
	source	"includes/ssl-server.erb"
	mode	"664"
	owner	"root"
	group	"root"
end

#
# Setup Default Server if there are no projects
#
if node.attribute?('projects')
	link "/etc/nginx/sites-enabled/default" do
		action :delete
		notifies :reload, resources(:service => 'nginx')
	end
else
	link "/etc/nginx/sites-enabled/default" do
		action :create
		link_type :symbolic
        to "/etc/nginx/sites-available/default"
		notifies :reload, resources(:service => 'nginx')
	end
end

#
# Setup logrotate
#
template "/etc/logrotate.d/nginx" do
	source	"logrotate/nginx.erb"
	mode	"644"
	owner	"root"
	group	"root"
end

#
# Reload logrotate config
#
execute "reload logrotate config" do
	command	<<-EOH
		sudo logrotate -f /etc/logrotate.conf
	EOH
end