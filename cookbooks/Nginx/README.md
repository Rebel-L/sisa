Nginx Cookbook
==============
This cookbook install Nginx webserver.

Requirements
------------
#### packages
- `System` - Nginx needs the System run before..

Attributes
----------
#### Nginx::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['nginx']['logrotate']['interval']</tt></td>
    <td>String</td>
    <td>The interval for logrotation of nginx logs.</td>
    <td><tt>daily</tt></td>
  </tr>
  <tr>
    <td><tt>['nginx']['logrotate']['number_of_files']</tt></td>
    <td>String</td>
    <td>The number of log files to keep.</td>
    <td><tt>10</tt></td>
  </tr>
  <tr>
    <td><tt>['nginx']['config']['sendfile']</tt></td>
    <td>String</td>
    <td>The configuration of the sendfile option.</td>
    <td><tt>on</tt></td>
  </tr>
</table>

Usage
-----
#### Nginx::default
Just include `Nginx` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Nginx]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
