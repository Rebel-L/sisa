# No default attributes so far
default['nginx']['logrotate']['interval']           = 'daily'
default['nginx']['logrotate']['number_of_files']    = '10'
default['nginx']['config']['sendfile']              = 'on'