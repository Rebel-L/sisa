Nginx CHANGELOG
===============

This file is used to list changes made in each version of the Nginx cookbook.

2.1.0
-----

- Rebel L - [Issue #6](https://bitbucket.org/Rebel-L/sisa/issues/6/ssl-support-for-nginx) Added basic ssl configuration.

1.6.0
-----

- Rebel L fixed the sendfile bug of [Issue #7](https://bitbucket.org/Rebel-L/sisa/issues/7/update-readme-files)

0.1.0
-----

- Rebel L - Initial release of Nginx
