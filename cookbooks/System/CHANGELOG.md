System CHANGELOG
================

This file is used to list changes made in each version of the LinuxUpdate cookbook.

3.0.0
-----

* Rebel L - Fixed iptables to work with Ubuntu 18.04
* Rebel L - ll alias has -h flag by default

2.1.0
-----

* Rebel L - Added recipe for htop

1.7.0
-----

* Rebel L - Moved LinuxUpdate Cookbook to System::apt
* Rebel L - added unzip recipe
* Rebel L - Moved Iptables cookbook to System::iptables
* Rebel L - Moved Git cookbook to System::git
* Rebel L - Moved Profil cookbook to System::profile

1.5.5
-----

* Rebel L - removed ppa install from this cookbook

0.1.0
-----

* Rebel L - Initial release of LinuxUpdate