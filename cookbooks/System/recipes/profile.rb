#
# Cookbook Name:: System
# Recipe:: profile
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
if node.chef_environment == 'development'
	node.default['System']['Profile']['host_color'] = node['System']['Profile']['color']['green']
else
	node.default['System']['Profile']['host_color'] = node['System']['Profile']['color']['red']
end

#
# Create .profile
#
node['System']['Profile']['users'].each do |user|
    xdebugSwitch = false
    fpmInstalled = false
    if node.chef_environment == 'development' && (node['recipes'].include?('Php::fpm') || node['recipes'].include?('Php::cli') || node['recipes'].include?('Php::composer'))
        xdebugSwitch = true
    end

    if node['recipes'].include?('Php::fpm')
        fpmInstalled = true
    end

	if user['name'] == 'root'
		node.default['System']['Profile']['user_color'] = node['System']['Profile']['color']['red']
	else
		node.default['System']['Profile']['user_color'] = node['System']['Profile']['color']['green']
	end

	template "#{user['home']}/.profile" do
		source	'.profile.erb'
		mode	'0644'
		owner	"#{user['name']}"
		group	"#{user['name']}"
		variables ({
			:home		    => user['home'],
			:host_color	    => node['System']['Profile']['host_color'],
			:user_color     => node['System']['Profile']['user_color'],
			:xdebugSwitch   => xdebugSwitch,
			:fpmInstalled   => fpmInstalled
		})
	end

	cookbook_file ".bashrc" do
    	path	"#{user['home']}/.bashrc"
    	action	:create
    	mode	'0644'
        owner	"#{user['name']}"
        group	"#{user['name']}"
    end
end
