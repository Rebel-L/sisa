#
# Cookbook Name:: Iptables
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# Install package
#
package "iptables" do
	action :install
end

#
# Create configuration file
#
template "/etc/iptables.up.rules" do
	source	"iptables.up.rules.erb"
	mode	"0644"
	owner	"root"
	group	"root"
end

execute "reload iptables rules" do
	command	<<-EOH
		sudo iptables-restore < /etc/iptables.up.rules
	EOH
end

#
# Ensure reload on sytem startup
#
cookbook_file "iptablesload.sh" do
	path	"/etc/network/if-pre-up.d/iptablesload"
	action	:create_if_missing
	owner	"root"
	group	"root"
	mode	"0775"
end
