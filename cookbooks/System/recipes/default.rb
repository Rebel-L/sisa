#
# Cookbook Name:: System
# Recipe:: default
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

include_recipe "System::apt"
include_recipe "System::iptables"
include_recipe "System::git"
include_recipe "System::profile"
include_recipe "System::htop"
