#
# Cookbook Name:: System (has been LinuxUpdate before)
# Recipe:: apt
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
execute "update source list" do
	command <<-EOH
		sudo apt-get update
	EOH
end

if node['System']['apt']['mode'] == "upgrade"
    execute "upgrade system" do
        command <<-EOH
            sudo apt-get upgrade -y
            sudo apt-get autoremove -y
            sudo apt-get autoclean -y
        EOH
    end
end