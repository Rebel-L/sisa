#
# Cookbook Name:: System
# Recipe:: htop
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# Install package
#
package "htop" do
	action :install
end