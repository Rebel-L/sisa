#
# Cookbook Name:: System
# Recipe:: unzip
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

package "unzip" do
	action :install
end