default['System']['Profile']['users']                 = []

# Color definitons
default['System']['Profile']['color']['green']		= '\[\e[32;1m\]'
default['System']['Profile']['color']['lightblue']	= '\[\e[36;1m\]'
default['System']['Profile']['color']['red']		= '\[\e[31;1m\]'
default['System']['Profile']['color']['yellow']		= '\[\e[33;1m\]'
default['System']['Profile']['color']['white']		= '\[\e[37;1m\]'
default['System']['Profile']['color']['reset']		= '\[\e[0m\]'
default['System']['Profile']['cdproj']              = '/vagrant'
