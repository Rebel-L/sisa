System Cookbook
===============
The cookbook manages the basic linux system components like apt, iptables, git etc.

Requirements
------------

No requirements so far.

Attributes
----------

| Key                                       | Type      | Description                                                               | Default               |
|-------------------------------------------|-----------|---------------------------------------------------------------------------|-----------------------|
| ['System']['Iptables']['WEBSERVER']       | String    | To open the default port 80 for http protocol. Can be 'On' or 'Off'       | Off                   |
| ['System']['Iptables']['WEBSERVERSSL']    | String    | To open the default SSL port 443 for https protocol. Can be 'On' or 'Off' | Off                   |
| ['System']['Iptables']['TCP']['Ports']    | Array     | A whitelist of individual ports to open.                                  | []                    |
| ['System']['Profile']['color']                      | String[]  | Color setup for the console.                                              | Many array entries.   |

Usage
-----
Just include `System` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[System]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
