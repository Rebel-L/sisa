# NodeJs CHANGELOG

This file is used to list changes made in each version of the NodeJs cookbook.

3.0.0
-----

* Rebel L - [Issue #31](https://bitbucket.org/Rebel-L/sisa/issues/31/upgrade-to-ubuntu-1804) Changed Default NodeJS version to 8.11.3 
* Rebel L - [Issue #31](https://bitbucket.org/Rebel-L/sisa/issues/31/upgrade-to-ubuntu-1804) Removed Bower Support
* Rebel L - [Issue #31](https://bitbucket.org/Rebel-L/sisa/issues/31/upgrade-to-ubuntu-1804) Yarn not installed by default anymore

2.0.0
-----

* Rebel L - [Issue #22](https://bitbucket.org/Rebel-L/sisa/issues/22/add-yarn-package-manager-to-nodejs) Changed package for yarn

1.8.0
-----

* Rebel L - added recipe for NodeJs::yarn
* Rebel L - switched to new NodeJS version 6.9.1

1.7.0
-----

* Rebel L - added recipe for NodeJs::bower
* Rebel L - added recipe for NodeJs::gulp
* Rebel L - switched to new NodeJS version 4.5.0

1.5.0
-----

* Rebel L - Initial release of NodeJs
