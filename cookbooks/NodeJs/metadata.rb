name             'NodeJs'
maintainer       'Rebel L'
maintainer_email 'dj@rebel-l.net'
license          'All rights reserved'
description      'Installs/Configures NodeJs'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '2.0.0'
