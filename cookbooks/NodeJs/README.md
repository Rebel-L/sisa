NodeJs Cookbook
=======================
This cookbook installs NodeJs to your system.

Requirements
------------
No requirements so far.

Attributes
------------

### NodeJs::default
		
| Key                   | Type      | Description                       | Default   |
|-----------------------|-----------|-----------------------------------|-----------|
| ['NodeJs']['gulp']    | Boolean   | Indicates to install gulp         | false     |
| ['NodeJs']['version'] | String    | The version of NodeJs to install  | 8.11.3    |
| ['NodeJs']['yarn']    | Boolean   | Indicates to install yarn         | false     |

Usage
------------

### NodeJs::default
Just include `NodeJs` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[NodeJs]"
  ]
}
```

Contributing
------------
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
------------

Authors: Rebel L

