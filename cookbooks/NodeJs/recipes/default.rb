#
# Cookbook Name:: NodeJs
# Recipe:: default
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#
bash "install nodejs #{node['NodeJs']['version']}" do
	user	"root"
	code	<<-EOH
	    wget -O /tmp/nodejs.tar.gz https://nodejs.org/dist/v#{node['NodeJs']['version']}/node-v#{node['NodeJs']['version']}-linux-x64.tar.gz
		tar -C /tmp/ -xzf /tmp/nodejs.tar.gz
		rm /tmp/nodejs.tar.gz
		mv /tmp/node-v#{node['NodeJs']['version']}-linux-x64 /opt/node
		mkdir /opt/node/etc
		echo 'prefix=/usr/local' > /opt/node/etc/npmrc
		ln -s /opt/node/bin/node /usr/local/bin/node
		ln -s /opt/node/bin/npm /usr/local/bin/npm
	EOH
	not_if	"node -v | grep #{node['NodeJs']['version']}"
end

if node['NodeJs']['yarn'] == true
    include_recipe "NodeJs::yarn"
end

if node['NodeJs']['gulp'] == true
    include_recipe "NodeJs::gulp"
end
