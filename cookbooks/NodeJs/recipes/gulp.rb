#
# Cookbook Name:: NodeJs
# Recipe:: gulp
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

bash "install gulp" do
    user	"root"
    code	<<-EOH
        npm -g install gulp
    EOH
    not_if "ls -l /usr/local/bin/gulp"
end
