#
# Cookbook Name:: NodeJs
# Recipe:: yarn
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

bash "trust gpg key from yarn" do
    user    "root"
	code    <<-EOH
		curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
	EOH
	not_if "apt-key list | grep yarn"
end

bash "add ppa repository" do
	user	"root"
	code	<<-EOH
	    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
	EOH
	not_if	"cat /etc/apt/sources.list.d/* | grep yarn"
end

bash "update source list" do
	user	"root"
	code	"apt-get update"
end

package "yarn" do
	action :install
end
