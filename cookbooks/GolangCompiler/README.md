GolangCompiler Cookbook
=======================
This cookbook installs the Golang compiler on your system.

Requirements
------------
No requirements so far.

Attributes
----------

#### GolangCompiler::default
| Key                   | Type      | Description                                    | Default                   |
|-----------------------|-----------|------------------------------------------------|---------------------------|
| ['Golang']['version'] | String    | The version of the Golang compiler to install. | 1.10.3                    |
| ['Golang']['gopath']  | String    | The path to the golang workspace.              | /home/vagrant/goworkspace |
| ['Golang']['project'] | String    | The project path after $GOPATH/src             |                           |


Usage
-----
#### GolangCompiler::default
Just include `GolangCompiler` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[GolangCompiler]"
  ]
}
```

Contributing
------------
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
