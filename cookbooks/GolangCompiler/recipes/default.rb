#
# Cookbook Name:: GolangCompiler
# Recipe:: default
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

bash "install golang compiler #{node['Golang']['version']}" do
	user	"root"
	code	<<-EOH
	    wget -O /tmp/go.tar.gz https://storage.googleapis.com/golang/go#{node['Golang']['version']}.linux-amd64.tar.gz
	    tar -C /usr/local -xzf /tmp/go.tar.gz
	    rm /tmp/go.tar.gz
	EOH
	not_if	"go version | grep #{node['Golang']['version']}"
end

i = 0
path = ""
p = node['Golang']['project'].split("/")
p.each do |t|
	if i < p.size - 1
		path = path + "/#{t}"
	end
    i = i + 1
end

# Create Goworkspace
bash "create go workspace" do
	user	"root"
	code	<<-EOH
	   mkdir -p #{node['Golang']['gopath']}/bin
	   mkdir -p #{node['Golang']['gopath']}/pkg
	   mkdir -p #{node['Golang']['gopath']}/src/#{path}
	   chown -R vagrant:root #{node['Golang']['gopath']}
	   ln -s /vagrant #{node['Golang']['gopath']}/src/#{node['Golang']['project']}
	EOH
	not_if { ::Dir.exists?("#{node['Golang']['gopath']}") }
end

node.default['System']['Profile']['cdproj'] = "#{node['Golang']['gopath']}/src/#{node['Golang']['project']}"
include_recipe "System::profile"

template "/etc/profile.d/go_compiler.sh" do
	source "go_compiler.sh.erb"
	mode "0755"
	owner "root"
	group "root"
end

include_recipe "GolangCompiler::glide"
