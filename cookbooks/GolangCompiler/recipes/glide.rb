#
# Cookbook Name:: GolangCompiler
# Recipe:: glide
#
# Copyright 2018, Rebel L
#
# All rights reserved - Do Not Redistribute
#

bash "add glide package manager for golang" do
	user	"root"
	code	<<-EOH
		export PATH=$PATH:/usr/local/go/bin
		export GOPATH=#{node['Golang']['gopath']}
        export GOBIN=$GOPATH/bin
        export PATH=$PATH:$GOBIN
		curl https://glide.sh/get | sh
	EOH
	not_if { ::File.directory?("#{node['Golang']['gopath']}/bin/glide") }
end
