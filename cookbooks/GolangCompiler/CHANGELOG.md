GolangCompiler CHANGELOG
========================

This file is used to list changes made in each version of the GolangCompiler cookbook.

3.0.0
-----

* Rebel L - [Issue #31](https://bitbucket.org/Rebel-L/sisa/issues/31/upgrade-to-ubuntu-1804) Fixed cookbook to work with Ubuntu 18.04
* Rebel L - [Issue #31](https://bitbucket.org/Rebel-L/sisa/issues/31/upgrade-to-ubuntu-1804) Introduced "project" attribute to setup goworkspace correct
* Rebel L - [Issue #31](https://bitbucket.org/Rebel-L/sisa/issues/31/upgrade-to-ubuntu-1804) Changed default Go version to 1.10.3

2.2.3
-----

* Rebel L - switched default version to 1.7.5
* Rebel L - added glide package manager

1.7.0
-----

* Rebel L - switched default version to 1.7

1.5.3
-----

* Rebel L - fixed package installation if vm doesn't exist.

1.5.2
-----

* Rebel L - Added the possibility to install Golang packages.

1.4.1
-----
* Rebel L - Added GOPATH to profile shell script.

1.3.4
-----
* Rebel L - Initial release of GolangCompiler
