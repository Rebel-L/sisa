default['Jenkins']['server_name'] 		= 'localhost'
default['Jenkins']['port'] 				= 8080
default['Jenkins']['install_plugins']	= true
default['Jenkins']['update_plugins']	= true
default['Jenkins']['plugin_url']		= "http://updates.jenkins-ci.org"
default['Jenkins']['plugin_path']		= "/var/lib/jenkins/plugins"
default['Jenkins']['plugins']			= [
	{
		"name"		=> "analysis-core.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "ansicolor.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "bitbucket.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "bitbucket-approve.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "build-monitor-plugin.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "config-file-provider.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "dashboard-view.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "email-ext.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "emailext-template.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "git.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "git-client.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "matrix-project.hpi",
		"version"	=> "latest"
	},
	{
		"name"		=> "phing.hpi",
		"version"	=> "latest"
	},
     	{
     		"name"		=> "token-macro.hpi",
     		"version"	=> "latest"
     	},
	{
		"name"		=> "uptime.hpi",
		"version"	=> "latest"
	}
]