#
# Cookbook Name:: Jenkins
# Recipe:: plugins-install
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# Install plugins
#
node['Jenkins']['plugins'].each do |plugin|
	plugin_url	= "#{node['Jenkins']['plugin_url']}/#{plugin['version']}/#{plugin['name']}"
	plugin_file	= "#{node['Jenkins']['plugin_path']}/#{plugin['name']}"

	execute "install jenkins plugin #{plugin['name']}" do
		user	"jenkins"
		group	"jenkins"
    	command	<<-EOH
    		wget #{plugin_url} -P #{node['Jenkins']['plugin_path']}
    	EOH
    	not_if do ::File.exists?(plugin_file) end
    	notifies :restart, resources(:service => 'jenkins')
    end
end