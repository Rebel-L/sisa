#
# Cookbook Name:: Jenkins
# Recipe:: plugins-update
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# Install plugin update script and execute it
#
cookbook_file "jenkins-update.sh" do
	path	"/usr/bin/jenkins-update.sh"
	action	:create_if_missing
	owner	"root"
	group	"jenkins"
	mode	"0775"
end

bash "jenkins plugin update" do
	user "jenkins"
	code "/usr/bin/jenkins-update.sh"
	notifies :restart, resources(:service => 'jenkins')
end