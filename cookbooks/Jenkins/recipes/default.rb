#
# Cookbook Name:: Jenkins
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "Java"
include_recipe "Nginx"
include_recipe "Php::composer"

#
# Installs key for jenkins repo and adds repo to source list
#
execute "trust gpg key from jenkins" do
	command <<-EOH
		wget -O- https://jenkins-ci.org/debian/jenkins-ci.org.key | apt-key add -
	EOH
	not_if "sudo apt-key list | grep Kohsuke"
end

execute "add jenkins repositories" do
	command	<<-EOH
		sudo add-apt-repository 'deb http://pkg.jenkins-ci.org/debian binary/'
	EOH
	not_if "cat /etc/apt/sources.list | grep jenkins-ci"
end

execute "update source list" do
	command <<-EOH
		sudo apt-get update
	EOH
end

#
# Install package
#
package "jenkins" do
	action :install
end

#
# Define Service
#
service "jenkins" do
	supports :restart => true
	action :enable
end

#
# Setup Nginx proxy
#
template "/etc/nginx/sites-available/jenkins" do
	source "jenkins.erb"
	mode "0644"
	owner "root"
	group "root"
end

link "/etc/nginx/sites-enabled/jenkins" do
	action :create
	link_type :symbolic
	to "/etc/nginx/sites-available/jenkins"
	notifies :reload, resources(:service => 'nginx')
end

#
# Allow Jenkins to use linux user credentials
#
execute "add shadow group to jenkins" do
	command	<<-EOH
		sudo usermod -a -G shadow jenkins
	EOH
	not_if "groups jenkins | grep shadow"
end

if node['Jenkins']['install_plugins'] == true
	include_recipe "Jenkins::plugins-install"
end

if node['Jenkins']['update_plugins'] == true
	include_recipe "Jenkins::plugins-update"
end