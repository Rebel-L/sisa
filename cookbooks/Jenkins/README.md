Jenkins Cookbook
================
This cookbook intalls a jenkins server.


Requirements
------------
- `Nginx` - Jenkins needs nginx.
- `Java` - Jenkins needs Java.
- `Php` - Php is needed for executing phpunit or phing.

Attributes
----------
#### Jenkins::default
<table>
	<tr>
		<th>['Jenkins']['server_name']</th>
		<th>String</th>
		<th>The domain where to reach the jenkins server.</th>
		<th>localhost</th>
	</tr>
	<tr>
		<th>['Jenkins']['port']</th>
		<th>Integer</th>
		<th>The port where to forward to.</th>
		<th>8080</th>
	</tr>
	<tr>
		<th>['Jenkins']['install_plugins']</th>
		<th>Boolean</th>
		<th>Flag to install plugins.</th>
		<th>true</th>
	</tr>
	<tr>
		<th>['Jenkins']['update_plugins']</th>
		<th>Boolean</th>
		<th>Flag to update plugins.</th>
		<th>true</th>
	</tr>
	<tr>
		<th>['Jenkins']['plugin_url']</th>
		<th>String</th>
		<th>Jenkins download url of plugins.</th>
		<th>http://updates.jenkins-ci.org</th>
	</tr>
	<tr>
		<th>['Jenkins']['plugin_path']</th>
		<th>String</th>
		<th>Path to jenkins plugins.</th>
		<th>/var/lib/jenkins/plugins</th>
	</tr>
	<tr>
		<th>['Jenkins']['plugins']</th>
		<th>Array</th>
		<th>A list of plugins to install.</th>
		<th>...</th>
	</tr>
</table>

Usage
-----
#### Jenkins::default
Just include `Jenkins` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Jenkins]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
