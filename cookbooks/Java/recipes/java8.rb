#
# Cookbook Name:: Java
# Recipe:: java8
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#

#
# Add Java8 repository
#
bash "add java 8 repo" do
	user	"root"
	code	"add-apt-repository -y ppa:webupd8team/java"
	not_if	"cat /etc/apt/sources.list.d/* | grep java"
end

bash "update source list" do
	user	"root"
	code	"apt-get update"
end

#
# Accept terms and conditions
#
if node['Java']['accept_terms'] == true
	bash "set accept terms" do
		user	"root"
		code	"echo debconf shared/accepted-oracle-license-v1-1 select true | debconf-set-selections"
		not_if	"debconf-show orasudo debconf-show oracle-java8-installer | grep 'shared/accepted-oracle-license-v1-1: true'"
	end

	bash "set license seen" do
		user	"root"
		code	"echo debconf shared/accepted-oracle-license-v1-1 seen true | debconf-set-selections"
		not_if	"debconf-show orasudo debconf-show oracle-java8-installer | grep 'shared/accepted-oracle-license-v1-1: true'"
	end
end

#
# Install Java 8
#
package "oracle-java8-installer" do
	action :install
end