#
# Cookbook Name:: Java
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
if node['Java']['version'] == 7
	include_recipe "Java::java7"
end

if node['Java']['version'] == 8
	include_recipe "Java::java8"
end