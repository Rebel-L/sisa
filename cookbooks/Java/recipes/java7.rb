#
# Cookbook Name:: Java
# Recipe:: java7
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
package "openjdk-7-jdk" do
	action :install
end
