Java Cookbook
=============
Installs the java sdk.


Requirements
------------
No requirements needed.

Attributes
----------
#### Java::default
<table>
	<tr>
		<th>['Java']['version']</th>
		<th>Integer</th>
		<th>The java version to install.</th>
		<th>8</th>
	</tr>
	<tr>
		<th>['Java']['accept_terms']</th>
		<th>Bool</th>
		<th>Acknowledges that you have read and accepted the orcale java license.</th>
		<th>false</th>
	</tr>
</table>

Usage
-----
Just include `Java` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Java]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
