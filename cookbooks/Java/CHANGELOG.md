Java CHANGELOG
==============

This file is used to list changes made in each version of the Java cookbook.

0.1.0
-----
- Rebel L - Initial release of Java

1.3.3
-----
- Rebel L - Added Java 8 recipe, moved default to Java 7 and make Java 8 default
