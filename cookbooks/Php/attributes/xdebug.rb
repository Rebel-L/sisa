#
# Xdebug Settings
#
default['Php']['xdebug']['remote_host']         = '127.0.0.1'
default['Php']['xdebug']['remote_port']         = 9000
default['Php']['xdebug']['remote_handler']      = 'dbgp'

default['Php']['xdebug']['profiler_enable']     = 0
default['Php']['xdebug']['profiler_output_dir'] = '/tmp/'