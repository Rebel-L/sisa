#
# Extensions
#
default['Php']['extensions']['curl']        = false
default['Php']['extensions']['mbstring']    = false
default['Php']['extensions']['soap']        = false
default['Php']['extensions']['sqlite']      = false
default['Php']['extensions']['xdebug']      = false
default['Php']['extensions']['xml']         = false
default['Php']['extensions']['xsl']         = false