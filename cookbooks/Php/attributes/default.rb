#
# Default settings
#
default['Php']['default']['version']                = '7.0'
default['Php']['default']['max_execution_time']		= 30
default['Php']['default']['memory_limit']			= '128M'
default['Php']['default']['error_reporting']		= 'E_ALL & ~E_DEPRECATED & ~E_STRICT'
default['Php']['default']['display_errors']			= 'Off'
default['Php']['default']['display_startup_errors']	= 'Off'
default['Php']['default']['track_errors']			= 'Off'
default['Php']['default']['docref']					= 'Off'
default['Php']['default']['date.timezone']			= "Europe/Berlin"

#
# Override Nginx settings
#
if node.attribute?('nginx')
	override["nginx"]["index"] = 'index.php index.html'
end


default['Php']['logrotate']['interval']			= 'daily'
default['Php']['logrotate']['number_of_files']	= '10'