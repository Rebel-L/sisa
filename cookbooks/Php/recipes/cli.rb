#
# Cookbook Name:: Php
# Recipe:: cli
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "Php::ppa-repository"

#
# Install package
#
package "php#{node['Php']['default']['version']}-cli" do
	action :install
end

#
# install extensions
#
include_recipe "Php::extensions"

#
# Create configuration file for cli
#
template "/etc/php/#{node['Php']['default']['version']}/cli/php.ini" do
	source "cli/php#{node['Php']['default']['version']}.ini.erb"
	mode "0644"
	owner "root"
	group "root"
end
