#
# Cookbook Name:: Php
# Recipe:: curl
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

package "php#{node['Php']['default']['version']}-curl" do
    action :install
end