#
# Cookbook Name:: Php
# Recipe:: sqlite
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

package "php#{node['Php']['default']['version']}-sqlite" do
    action :install
end