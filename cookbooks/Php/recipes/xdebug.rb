#
# Cookbook Name:: Php
# Recipe:: xdebug
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

if node.chef_environment == "development" || node['Php']['phpunit']['force'] == true
    #
    # install Xdebug package
    #
    package "php-xdebug" do
        action :install
    end

    #
    # Create configuration
    #
    template "/etc/php/#{node['Php']['default']['version']}/mods-available/xdebug.ini" do
    	source "xdebug/xdebug.ini.erb"
    	mode "0644"
    	owner "root"
    	group "root"
    end

    #
    # Disable Xdebug by default because of performance
    #
    if node['recipes'].include?('Php::fpm')
        bash "disable xdebug" do
            user	"root"
            code	<<-EOH
                phpdismod xdebug
            EOH
            notifies :restart, resources(:service => "php#{node['Php']['default']['version']}-fpm")
        end
    else
        bash "disable xdebug" do
            user	"root"
            code	<<-EOH
                phpdismod xdebug
            EOH
        end
    end
end