#
# Cookbook Name:: Php
# Recipe:: fpm
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "Php::ppa-repository"

#
# Install package
#
package "php#{node['Php']['default']['version']}-fpm" do
	action :install
end

#
# Define Service
#
service "php#{node['Php']['default']['version']}-fpm" do
	supports :restart => true
	action :enable
end

#
# install extensions
#
include_recipe "Php::extensions"

#
# Create configuration file for fpm
#
template "/etc/php/#{node['Php']['default']['version']}/fpm/php.ini" do
	source "fpm/php#{node['Php']['default']['version']}.ini.erb"
	mode "0644"
	owner "root"
	group "root"
	notifies :restart, resources(:service => "php#{node['Php']['default']['version']}-fpm")
end

#
# Activate fpm in nginx
#
if node.attribute?('nginx')
	template "/etc/nginx/conf.d/php-fpm.conf" do
    	source "nginx/php-fpm-nginx.conf.erb"
    	mode "0644"
    	owner "root"
    	group "root"
    	notifies :restart, resources(:service => 'nginx')
    end
end

#
# Setup logrotate
#
template "/etc/logrotate.d/php#{node['Php']['default']['version']}-fpm" do
	source	"logrotate/php-fpm.erb"
	mode	"644"
	owner	"root"
	group	"root"
end

#
# Reload logrotate config
#
execute "reload logrotate config" do
	command	<<-EOH
		sudo logrotate -f /etc/logrotate.conf
	EOH
end
