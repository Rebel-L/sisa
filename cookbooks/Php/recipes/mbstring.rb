#
# Cookbook Name:: Php
# Recipe:: mbstring
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

package "php#{node['Php']['default']['version']}-mbstring" do
    action :install
end