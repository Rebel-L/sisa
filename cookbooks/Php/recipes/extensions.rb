#
# Cookbook Name:: Php
# Recipe:: extensions
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

include_recipe "Php::phpunit"

#
# cURL
#
if node['Php']['extensions']['curl'] == true
    include_recipe "Php::curl"
end

#
# mbstring
#
if node['Php']['extensions']['mbstring'] == true
    include_recipe "Php::mbstring"
end

#
# soap
#
if node['Php']['extensions']['soap'] == true
    include_recipe "Php::soap"
end

#
# sqlite
#
if node['Php']['extensions']['sqlite'] == true
    include_recipe "Php::sqlite"
end

#
# xdebug
#
if node['Php']['extensions']['xdebug'] == true
    include_recipe "Php::xdebug"
end

#
# xml
#
if node['Php']['extensions']['xml'] == true
    include_recipe "Php::xml"
end

#
# xsl
#
if node['Php']['extensions']['xsl'] == true
    include_recipe "Php::xsl"
end
