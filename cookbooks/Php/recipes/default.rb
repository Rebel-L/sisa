#
# Cookbook Name:: Php
# Recipe:: default
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "Php::fpm"
include_recipe "Php::composer"	# includes also Php:cli