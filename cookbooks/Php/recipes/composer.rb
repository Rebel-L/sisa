#
# Cookbook Name:: Php
# Recipe:: composer
#
# Copyright 2015, Rebel L
#
# All rights reserved - Do Not Redistribute
#
include_recipe "Php::cli"

#
# Installs composer for php
#
execute "install composer for php" do
	command	<<-EOH
		php -r "readfile('https://getcomposer.org/installer');" | php
	EOH
end

execute "move composer to global directory" do
	command	<<-EOH
		mv composer.phar /usr/local/bin/composer
	EOH
end

#
# Composer needs unzip
#
include_recipe "System::unzip"
