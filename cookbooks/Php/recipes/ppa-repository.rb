#
# Cookbook Name:: Php
# Recipe:: ppa-repository
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

bash "remove old ppa repository" do
	user	"root"
	code	<<-EOH
		add-apt-repository -y -r ppa:ondrej/php5-5.6
	EOH
	only_if	"grep 'ppa' /etc/apt/sources.list.d/* | grep 'php5-5.6'"
end

bash "add ppa repository" do
	user	"root"
	code	<<-EOH
		add-apt-repository -y ppa:ondrej/php
		apt-get update
	EOH
	not_if	"grep 'ppa' /etc/apt/sources.list.d/* | grep '/php/'"
end

cookbook_file "ppa-key" do
	path	"/tmp/ppa-key"
	action	:create_if_missing
	owner	"root"
	mode	"0775"
	not_if  "apt-key list | grep '1024R/E5267A6C'"
end

bash "add ppa key" do
    user    "root"
    code    <<-EOH
        apt-key add /tmp/ppa-key
        rm /tmp/ppa-key
    EOH
    not_if  "apt-key list | grep '1024R/E5267A6C'"
end