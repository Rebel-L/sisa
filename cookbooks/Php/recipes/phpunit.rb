#
# Cookbook Name:: Php
# Recipe:: phpunit
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

`grep 'phpunit/phpunit' /vagrant/composer.json`
phpunit = $?.exitstatus
if node['Php']['phpunit']['force'] == true || phpunit == 0
    node.default['Php']['extensions']['soap']   = true
    node.default['Php']['extensions']['xdebug'] = true
    node.default['Php']['extensions']['xml']    = true
end
