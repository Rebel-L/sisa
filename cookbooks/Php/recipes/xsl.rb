#
# Cookbook Name:: Php
# Recipe:: xsl
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

package "php#{node['Php']['default']['version']}-xsl" do
    action :install
end