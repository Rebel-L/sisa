#
# Cookbook Name:: Php
# Recipe:: xml
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

package "php#{node['Php']['default']['version']}-xml" do
    action :install
end