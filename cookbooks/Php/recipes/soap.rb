#
# Cookbook Name:: Php
# Recipe:: soap
#
# Copyright 2016, Rebel L
#
# All rights reserved - Do Not Redistribute
#

package "php#{node['Php']['default']['version']}-soap" do
    action :install
end