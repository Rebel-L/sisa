Php Cookbook
============
This cookbook installs PHP on your system. It includes also recipes to install composer. Supported PHP Versions are:

* 5.5.x
* 5.6.x
* 7.0.x

You are not able to decide which patch version to use, it's always the newest stable version.

The following extensions will be installed automatically in **development** environment:

* xdebug (but disabled by default)

Requirements
------------

No requirements so far.

Attributes
----------
#### Php::default

|   Key                                             |   Type    |   Description                                                         |   Default                             |
|---------------------------------------------------|-----------|-----------------------------------------------------------------------|---------------------------------------|
|   ['Php']['default']['version']                   |   String  |   The PHP version to install. Possible values are 5.5, 5.6 or 7.0.    |   7.0                                 |
|   ['Php']['default']['max_execution_time']        |   Integer |   The maximum execution time of a script.                             |   30                                  |
|   ['Php']['default']['memory_limit']              |   String  |   The limit of the used memory.                                       |   128M                                |
|   ['Php']['default']['error_reporting']           |   String  |   The level of error reporting                                        |   E_ALL & ~E_DEPRECATED & ~E_STRICT   |
|   ['Php']['default']['display_errors']            |   String  |   Setting to display errors.                                          |   'Off'                               |
|   ['Php']['default']['display_startup_errors']    |   String  |   Setting to display errors on startup.                               |   'Off'                               |
|   ['Php']['default']['track_errors']              |   String  |   Setting to track errors.                                            |   'Off'                               |
|   ['Php']['default']['docref']                    |   String  |   Setting to add references to documentation.                         |   'Off'                               |
|   ['Php']['default']['date.timezone']             |   String  |   The default timezone.                                               |   "Europe/Berlin"                     |


#### Php::cli

Has the same as *Php::default*, but overrides some defaults.

|   Key                                             |   Type    |   Description                                                         |   Default                             |
|---------------------------------------------------|-----------|-----------------------------------------------------------------------|---------------------------------------|
|   ['Php']['cli']['memory_limit']                  |   String  |   The limit of the used memory.                                       |   -1                                  |


#### Php::fpm

Has the same as *Php::default*.


#### Php::extensions

|   Key                                             |   Type    |   Description                                                                 |   Default                             |
|---------------------------------------------------|-----------|-------------------------------------------------------------------------------|---------------------------------------|
|   ['Php']['extensions']['curl']                   |   Boolean |   Installs the cURL extension                                                 |   false                               |
|   ['Php']['extensions']['mbstring']               |   Boolean |   Installs the mbstring extension                                             |   false                               |
|   ['Php']['extensions']['soap']                   |   Boolean |   Installs the soap extension. Can be overriden if phpunit is installed.      |   false                               |
|   ['Php']['extensions']['sqlite']                 |   Boolean |   Installs the sqlite extension                                               |   false                               |
|   ['Php']['extensions']['xdebug']                 |   Boolean |   Installs the xdebug extension. Can be overriden if phpunit is installed.    |   false                               |
|   ['Php']['extensions']['xml']                    |   Boolean |   Installs the xml extension. Can be overriden if phpunit is installed.       |   false                               |
|   ['Php']['extensions']['xsl']                    |   Boolean |   Installs the xsl extension                                                  |   false                               |


#### Php::phpunit

|   Key                                             |   Type    |   Description                                                         |   Default                             |
|---------------------------------------------------|-----------|-----------------------------------------------------------------------|---------------------------------------|
|   ['Php']['phpunit']['force']                     |   Boolean |   Forces installing phpunit dependencies                              |   false                               |


#### Php::xdebug

|   Key                                             |   Type    |   Description                                                         |   Default                             |
|---------------------------------------------------|-----------|-----------------------------------------------------------------------|---------------------------------------|
|   ['Php']['xdebug']['remote_host']                |   String  |   The debugging remote host                                           |   127.0.0.1                           |
|   ['Php']['xdebug']['remote_port']                |   Integer |   The port of the debugging remote host                               |   9000                                |
|   ['Php']['xdebug']['remote_handler']             |   String  |   The handler of the debugging remote host                            |   dbgp                                |
|   ['Php']['xdebug']['profiler_enable']            |   Integer |   Enables the profiling. Zero means it is turned off.                 |   0                                   |
|   ['Php']['xdebug']['profiler_output_dir']        |   String  |   The output directory of the profiler                                |   /tmp/                               |


Usage
-----
#### Php::default
Just include `Php` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[Php]"
  ]
}
```

Contributing
------------

1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: Rebel L
