Php CHANGELOG
=============

This file is used to list changes made in each version of the Php cookbook.

1.7.0
-----

* Rebel L - added dependencies for phpunit 
* Rebel L - added recipe for curl
* Rebel L - added recipe for mbstring
* Rebel L - added recipe for sqlite
* Rebel L - added recipe for xsl
* Rebel L - refactored Php::phpunit and the depending extensions Php:soap, Php:xdebug and Php:xml

1.6.0
-----

* Rebel L - Fixed a bug with the PPA repository
* Rebel L - Support added for PHP 5.5, 5.6 & 7.0
* Rebel L - Make PHP 7.0 the default 
* Rebel L - Added xdebug extension

1.5.5
-----

* Rebel L - Change ppa repositor to _ppa:ondrej/php_

0.1.0
-----

* Rebel L - Rebel L - Initial release of Php
