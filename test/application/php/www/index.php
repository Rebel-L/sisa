<?php
/**
 * This is just a test script for a www website.
 *
 * @author: Rebel L <dj@rebel-l.net>
 *
 * Date: 18.12.2016
 * Time: 14:55
 */
$sentence = 'Hello WWW!';
?>

<html>
	<head>
		<title>WWW</title>
		<link href="//assets.sisa-web.dev/main.css" rel="stylesheet">
		<script src="//assets.sisa-web.dev/hello.js"></script>
	</head>
	<body>
		<img src="//images.sisa-web.dev/banner.png">
		<h1><?= $sentence ?></h1>
		<p>
			Did you get the alert message from Javascript? If not something went wrong.
		</p>
	</body>
</html>
