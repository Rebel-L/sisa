<?php
/**
 * This is just a test script for a simple website.
 *
 * @author: Rebel L <dj@rebel-l.net>
 *
 * Date: 18.12.2016
 * Time: 14:55
 */
$sentence = 'Hello World!';
?>

<html>
	<head>
		<title>Hello</title>
	</head>
	<body>
		<h1><?= $sentence ?></h1>
	</body>
</html>
