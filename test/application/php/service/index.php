<?php
/**
 * This is just a test script for a php service
 *
 * @author: Rebel L <dj@rebel-l.net>
 *
 * Date: 18.12.2016
 * Time: 14:43
 */

$sentence = new stdClass();
$sentence->sentence = 'Hello World!';

header('Content-Type: application/json');
echo json_encode($sentence);