# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
	# Setup for all machines
	config.vm.provision "shell", inline: "echo Starting Vagrant ..."
	config.vm.box = "Ubuntu1804"
    config.vm.box_url = "https://www.dropbox.com/s/lq0a011b8xhddpg/ubuntu1804lts5212.box?dl=1"
	config.ssh.insert_key = false	# Avoid that vagrant removes default insecure key

	# Host manager setup
	config.hostmanager.enabled				= true
	config.hostmanager.manage_host			= true
	config.hostmanager.manage_guest			= true
	config.hostmanager.ignore_private_ip	= false
	config.hostmanager.include_offline		= true

	# Provider-specific configuration so you can fine-tune various
	# backing providers for Vagrant. These expose provider-specific options.
	# Example for VirtualBox:
	#
 	config.vm.provider "virtualbox" do |vb|
# 		# Don't boot with headless mode
# 		vb.gui = true
#
# 		# Use VBoxManage to customize the VM. For example to change memory:
# 		vb.customize ["modifyvm", :id, "--memory", "1024"]
		vb.memory = 384
 	end

	# Setup for AllInOne machine only
	config.vm.define "AllInOne", autostart: false do |allinone|
		# Setup network
		allinone.vm.network "private_network", ip: "192.168.33.101"
		allinone.vm.hostname = 'sisa-allinone.dev'

		allinone.vm.provider "virtualbox" do |vb|
			vb.name = "SiSa_AllInOne"
		end

		# Chef Configuration
		allinone.vm.provision "chef_solo" do |chef|
			chef.cookbooks_path = "./cookbooks"
			chef.roles_path = "./roles"
			chef.environments_path = "./environments"
			chef.data_bags_path = "./data_bags"
			chef.add_role "AllInOne"
			chef.environment = "development"

			# You may also specify custom JSON attributes:
# 			chef.json = {
# 				'projects' => [
# 					{
# 						'name'			=> 'ddr',
# 						'type'			=> 'php',
# 						'server_name'	=> 'ddr.localparty',
# 						'root'			=> '/vagrant/public',
# 						'index'			=> 'index.php'
# 					},
# 					{
# 						'name'			=> 'ddr_redirect',
# 						'type'			=> 'redirect',
# 						'server_name'	=> 'ddr.local',
# 						'target'		=> 'ddr.localparty'
# 					}
# 				]
# 			}
		end
	end

	# Setup for Web server only
	config.vm.define "Web", autostart: false do |web|
		# Setup network
		web.vm.network "private_network", ip: "192.168.33.21"
		web.vm.hostname = 'sisa-web.dev'
		web.hostmanager.aliases = %w(
		    www.sisa-web.dev assets.sisa-web.dev images.sisa-web.dev
		    simple.sisa-web.dev www.simple.sisa-web.dev
		    service.sisa-web.dev
		    redirect.sisa-web.dev
		)

		web.vm.provider "virtualbox" do |vb|
			vb.name = "SiSa_WebServer"
		end

		# Chef Configuration
		web.vm.provision "chef_solo" do |chef|
			chef.cookbooks_path = "./cookbooks"
			chef.roles_path = "./roles"
			chef.environments_path = "./environments"
			chef.data_bags_path = "./data_bags"
			chef.add_role "WebServer"
			chef.environment = "development"
			chef.add_recipe "Php"
# 			chef.add_recipe "LetsEncrypt"

			# Custom json attributes
			chef.json = {
# 			    'Php' => {
# 			        'extensions' => {
# 			            'curl' => true,
# 			            'mbstring' => true,
#                         'sqlite' => true,
#                         'xsl' => true
# 			        }
# 			        'default' => {
# 			            'version' => '5.6'
# 			        }
#                     'phpunit'   => {
#                         'force' => true
#                     }
# 			    },
#                 'LetsEncrypt' => {
#                     'domains' => [
#                         {
#                             'dns' => 'www.ddr.dev',
#                             'webroot' => '/vagrant/test/application/php/www'
#                         },
#                         {
#                             'dns' => 'assets.ddr.dev',
#                             'webroot' => '/vagrant/test/application/php/www/assets'
#                         }
#                     ]
#                 },
				'projects' => [
					{
						'name'			=> 'service.sisa-web.dev',
						'type'			=> 'service',
						'server_name'	=> 'service.sisa-web.dev',
						'root'			=> '/vagrant/test/application/php/service',
						'index'			=> 'index.php'
					},
					{
                        'name'			=> 'simple.sisa-web.dev',
                        'type'			=> 'php-simple',
                        'server_name'	=> 'simple.sisa-web.dev',
                        'root'			=> '/vagrant/test/application/php/simple',
                        'index'			=> 'index.php'
                    },
                    {
                        'name'			=> 'sisa-web.dev',
                        'type'			=> 'php-www',
                        'server_name'	=> 'sisa-web.dev',
                        'root'			=> '/vagrant/test/application/php/www',
                        'index'			=> 'index.php'
                    },
#                     {
#                         'name'			=> 'sisa-web-ssl.dev',
#                         'type'			=> 'php-ssl',
#                         'server_name'	=> 'sisa-web.dev',
#                         'root'			=> '/vagrant/test/application/php/www',
#                         'index'			=> 'index.php'
#                     },
                    {
                         'name'			=> 'redirect.sisa-web.dev',
                         'type'			=> 'redirect',
                         'server_name'	=> 'redirect.sisa-web.dev',
                         'target'		=> 'simple.sisa-web.dev',
                    },
#                     {
#                          'name'			=> 'redirect.sisa-web-ssl.dev',
#                          'type'			=> 'redirect',
#                          'server_name'	=> 'redirect.sisa-web.dev',
#                          'target'		=> 'simple.sisa-web.dev',
#                          'ssl'          => true
#                     }
				]
			}
		end
	end

	# Setup for continous integration server only
	config.vm.define "CIS", autostart: false do |cis|
		# Setup network
		cis.vm.network "private_network", ip: "192.168.33.31"
		cis.vm.hostname = 'sisa-cis.dev'

		cis.vm.provider "virtualbox" do |vb|
			vb.name = "SiSa_CIServer"
		end

		# Chef Configuration
		cis.vm.provision "chef_solo" do |chef|
			chef.cookbooks_path = "./cookbooks"
			chef.roles_path = "./roles"
			chef.environments_path = "./environments"
			chef.data_bags_path = "./data_bags"
			chef.add_role "CiServer"
			chef.environment = "development"

			# Custom json attributes
			chef.json = {
				'Java' => {
					'accept_terms'	=> true
				},
				'Jenkins' => {
					'server_name'	=> 'jenkins.local'
				}
			}
		end
	end

	# Setup for continous integration server only
	config.vm.define "DbMongo", autostart: false do |dbmongo|
		# Setup network
		dbmongo.vm.network "private_network", ip: "192.168.33.41"
		dbmongo.vm.hostname = 'sisa-dbmongo.dev'

		dbmongo.vm.provider "virtualbox" do |vb|
			vb.name = "SiSa_DbMongo"
		end

		# Chef Configuration
		dbmongo.vm.provision "chef_solo" do |chef|
			chef.cookbooks_path = "./cookbooks"
			chef.roles_path = "./roles"
			chef.environments_path = "./environments"
			chef.data_bags_path = "./data_bags"
			chef.add_role "DbMongo"
			chef.environment = "development"

			# Custom json attributes
# 			chef.json = {
# 				'Jenkins' => {
# 					'server_name'	=> 'jenkins.local'
# 				}
# 			}
		end
	end

	# Setup for index server only
	config.vm.define "IndexServer", autostart: false do |is|
		# Setup network
		is.vm.network "private_network", ip: "192.168.33.51"
		is.vm.hostname = 'sisa-is.dev'

		is.vm.provider "virtualbox" do |vb|
			vb.name = "SiSa_IndexServer"
		end

		# Chef Configuration
		is.vm.provision "chef_solo" do |chef|
			chef.cookbooks_path = "./cookbooks"
			chef.roles_path = "./roles"
			chef.environments_path = "./environments"
			chef.data_bags_path = "./data_bags"
			chef.add_role "IndexServer"
			chef.environment = "development"

			# Custom json attributes
			chef.json = {
				'Java' => {
					'accept_terms'	=> true
				}
			}
		end
	end

	# Setup for golang service only
    config.vm.define "GoService", autostart: false do |gs|
    	# Setup network
        gs.vm.network "private_network", ip: "192.168.33.61"
		gs.vm.hostname = 'sisa-go.dev'

		gs.vm.provider "virtualbox" do |vb|
			vb.name = "SiSa_GoService"
		end

        # Chef Configuration
        gs.vm.provision "chef_solo" do |chef|
            chef.cookbooks_path = "./cookbooks"
            chef.roles_path = "./roles"
            chef.environments_path = "./environments"
            chef.data_bags_path = "./data_bags"
            chef.add_role "Default"
            chef.environment = "development"
            chef.add_recipe "GolangCompiler"
            chef.add_recipe "Memcached"

            # Custom json attributes
            chef.json = {
                'Golang' => {
                    'project' => 'github.com/rebel-l/sisa'
                },
                'memcached' => {
                    'memory'	=> '32'
                },
                'System' => {
                    'Iptables' => {
                        'TCP'	=> {
                            'Ports' => [8080]
                        }
                    }
				}
            }
        end
    end

	# Setup for nodejs service only
    config.vm.define "NodeJsService", autostart: false do |njss|
    	# Setup network
        njss.vm.network "private_network", ip: "192.168.33.62"
        njss.vm.hostname = 'sisa-nodejs.dev'

		njss.vm.provider "virtualbox" do |vb|
			vb.name = "SiSa_NodeJsService"
		end

        # Chef Configuration
        njss.vm.provision "chef_solo" do |chef|
            chef.cookbooks_path = "./cookbooks"
            chef.roles_path = "./roles"
            chef.environments_path = "./environments"
            chef.data_bags_path = "./data_bags"
            chef.add_role "Default"
            chef.environment = "development"
            chef.add_recipe "NodeJs"

			chef.json = {
# 			    'NodeJs'    => {
#                     'yarn' => true,
#                     'gulp' => true
#                 },
				'System' => {
                    'Iptables' => {
                        'TCP'	=> {
                            'Ports' => [8080]
                        }
                    }
                }
			}
        end
    end

    # Setup for toolbox only
	config.vm.define "ToolBox", autostart: false do |toolbox|
		# Setup network
		toolbox.vm.network "private_network", ip: "192.168.33.253"
		toolbox.vm.hostname = 'sisa-toolbox.dev'

		toolbox.vm.provider "virtualbox" do |vb|
        	vb.name = "SiSa_ToolBox"
        end

		# Chef Configuration
		toolbox.vm.provision "chef_solo" do |chef|
			chef.cookbooks_path = "./cookbooks"
			chef.roles_path = "./roles"
			chef.environments_path = "./environments"
			chef.data_bags_path = "./data_bags"
			chef.add_role "ToolBox"
			chef.environment = "development"

			chef.add_recipe "Docker"
		end
	end

	# Setup for docker server
    config.vm.define "DockerBox", autostart: false do |dockerbox|
        # Setup network
        dockerbox.vm.network "private_network", ip: "192.168.33.251"
        dockerbox.vm.hostname = 'sisa-dockerbox.dev'

        dockerbox.vm.provider "virtualbox" do |vb|
            vb.name = "SiSa_DockerBox"
            vb.customize ["modifyvm", :id, "--memory", "1024"]
        end

        # Chef Configuration
        dockerbox.vm.provision "chef_solo" do |chef|
            chef.cookbooks_path = "./cookbooks"
            chef.roles_path = "./roles"
            chef.environments_path = "./environments"
            chef.data_bags_path = "./data_bags"
            chef.add_role "DockerServer"
            chef.environment = "development"
            chef.add_recipe "Redis::tools"

            chef.json = {
                'System' => {
                    'Iptables' => {
                        'TCP'	=> {
                            'Ports' => [6379]
                        }
                    }
                }
            }
        end
    end
end
