SiSA - Simple Server Architecture
=================================

This project is designed for a simple start with private web applications unsing PHP, Nginx, several databases, continuous integration etc. It is maybe useful for small startups at the very first beginning. It's containing several chef cookbooks to setup up several servers with different roles or just create a developer box with everything installed on it.
For more details you should have a look into the other readme files you find in the subdirectories. This project contains my basic cookbooks for setting up webapplication based servers for different environments like development, testing, staging and production.

To setup your web application server configuration for Nginx, the "Projects" cookbook is used.

Requirements
============
First of you should have a basic understanding of chef. If you want to use it with Vagrant and VirtualBox you should 
have Knowledge with these technologies or have a look into the [Wiki](https://bitbucket.org/Rebel-L/sisa/wiki/).

Contains
========

You'll find cookbooks for the following applications:

* CUPS
* Docker
* ChefDK
* ElasticSearch
* FOP
* Git
* Golang Compiler
* Java 7 & 8
* Jenkins
* Memcached
* MongoDB
* NewRelic
* Nginx
* NodeJS
* PHP 5.5, 5.6 and 7.0 for CLI and FPM. Also supports Composer
* Projects (it's used for the nginx server configuration for your individual applications)
* Redis

More information see the [changelog](CHANGELOG.md).

Examples
========

You find several examples of different server roles and setups in the "Vagrantfile" in the root directory.


Setup
==================

You can setup your project with SiSa using Composer, Bower or NPM. For more details have a look in the [wiki pages](https://bitbucket.org/Rebel-L/sisa/wiki/Use%20SiSa%20In%20Your%20Project).
My experience with vagrant showed, after adding Chef cookbooks the first time, you to need to recreate your VM (Windows host). But you can try to run `vagrant provision` only.


Original Chef Readme
====================

More information about handling with Chef you'll find in the [README-Chef.md](README-Chef.md).