Changelog - SiSa
================

3.0.0
-----
* Rebel L - New Version for Ubuntu 18.04
* Rebel L - Fixed iptables to work with Ubuntu 18.04
* Rebel L - Default Go Version switched to Version 1.10.3
* Rebel L - Default NodeJS Version switched to 8.11.3
* Rebel L - Bower is not supported anymore
* Rebel L - Yarn installation deactivated by default

2.3.1
-----
* Rebel L - Make Docker compose version configurable

2.3.0
-----
* Rebel L - [Issue #30](https://bitbucket.org/Rebel-L/sisa/issues/30/redis-cookbook-needed) Initial release of Redis
* Rebel L - [Issue #30](https://bitbucket.org/Rebel-L/sisa/issues/30/redis-cookbook-needed) Initial release of ChefDK

2.2.4
-----
* Rebel L - added docker server role

2.2.3
-----
* Rebel L - switched default version to 1.7.5
* Rebel L - added glide package manager

2.2.2
-----
* Rebel L - changed description in package.json and composer.json

2.2.1
-----
* Rebel L - Added docker compose

2.2.0
-----
* Rebel L - [Issue #25](https://bitbucket.org/Rebel-L/sisa/issues/25/create-docker-cookbook) Initial release of Docker

2.1.0
-----
* Rebel L - Added recipe for htop and install it as default
* Rebel L - [Issue #6](https://bitbucket.org/Rebel-L/sisa/issues/6/ssl-support-for-nginx) Added LetsEncrypt Cookbook for SSL support
* Rebel L - [Issue #6](https://bitbucket.org/Rebel-L/sisa/issues/6/ssl-support-for-nginx) Added SSL support

2.0.0
-----
* Rebel L - [Issue #24](https://bitbucket.org/Rebel-L/sisa/issues/24/switch-to-ubuntu-1604) Switched to Ubuntu 16.04 base box
* Rebel L - Added test projects for the Project cookbook
* Rebel L - [Issue #23](https://bitbucket.org/Rebel-L/sisa/issues/23/nginx-project-type-service-duplicate) Fixed the duplicate location error
* Rebel L - [Issue #22](https://bitbucket.org/Rebel-L/sisa/issues/22/add-yarn-package-manager-to-nodejs) Changed package for yarn

1.8.0
-----
* Rebel L - added recipe for NodeJs::yarn
* Rebel L - switched to new NodeJS version 6.9.1

1.7.0
-----
* Rebel L - Added Php::soap extension
* Rebel L - Added Php::xml extension
* Rebel L - Added dependencies for phpunit
* Rebel L - Refactored LinuxUpdate cookbook and moved it to System
* Rebel L - Added recipe System::unzip
* Rebel L - moved *Startup* cookbook to *System* cookbook as recipe
* Rebel L - Added cookbook Fop
* Rebel L - Added recipe Php::curl
* Rebel L - Added recipe Php::mbstring
* Rebel L - Added recipe Php::sqlite
* Rebel L - Added recipe Php::xsl
* Rebel L - Added cookbook Cups
* Rebel L - NodeJS: changed default version to 4.5.0
* Rebel L - refactored Php::phpunit and the depending extensions Php:soap, Php:xdebug and Php:xml
* Rebel L - GolangCompiler: changed default version to 1.7

1.6.1
-----
* Rebel L - Hotfix on dependency of Php::fpm

1.6.0
-----
* Rebel L - Fixed a bug with the PPA repository
* Rebel L - Support added for PHP 5.5, 5.6 & 7.0
* Rebel L - Make PHP 7.0 the default 
* Rebel L - Added xdebug extension
* Rebel L - fixed send_file problem
* Rebel L - Added shortcut for switching xdebug

1.5.4
-----
* added development aliases

1.5.3
-----
* fixed package installation if vm doesn't exist.

1.5.2
-----
* added the possibility to install Golang packages.

1.5.1
-----
* added hostname management
* setup gui names
* added download link for used vagrant box
* added documentation

1.5.0
-----
* added port whitelist to open them for e.g. services
* added cookbook for NodeJs
* removed unnnessary command from Golang cookbook
* make port 8080 available for GoService

1.4.1
-----
* Added GOPATH to profile shell script.

1.4.0
-----
Added cookbooks for:
* ApacheBenchmark (apache2-utils)

Added roles:
* ToolBox (installing ApacheBenchmark)

1.3.5
-----
Cleaned up readme and introduced this chanelog.

1.3.4
-----
Added cookbooks for:
* Golang compiler
* Memcached

1.3.3
-----
Added cookbooks for:
* ElasticSearch
* MongoDB (single server)

Cleanup for several cookbook docuentations.

1.3.2
-----
Some optimizations for composer.

1.3.1
-----
Renaming of this project and cleanup.

1.3.0
-----
Added cookbooks for:
* Git
* Java
* Jenkins

Did some minor bugfixes and optimizations.

1.2.1
-----
Fixed Composer setup.

1.2.0
-----
Added recipes for:

* Composer (in PHP cookbook)

1.1.1
-----
Bugfix for PHP5-CLI.

1.1.0
-----
Added cookbooks for:
* NewRelic agent

0.1.0
-----
Contains cookbooks for:
* Iptables
* System (apt-get update/upgrade)
* Nginx
* Php
* Bash .profile
* Projects (Nginx configs)
* Startup Scripts (restoring Iptables)
